<?php

use Hkw\TencentLatlng\Http\Controllers;
use Illuminate\Support\Facades\Route;

Route::get('tencent-latlng', Controllers\TencentLatlngController::class.'@index');