# Dcat Admin 腾讯地图选点Form扩展

### 功能说明
从laravel-admin那边移植过来，支持地点检索

#### 配置：在admin.php 配置文件下增加一项配置（Dcat Admin的地图配置）

```php
'map' => [
    'provider' => 'tencent',
    'keys' => [
        'tencent' => env('TENCENT_MAP_API_KEY'),
    ],
],
```

### 使用方法
1. 在bootstrap.php加入以下代码
```php
\Hkw\TencentLatlng\Form\TencentLatlng::requireAssets();
```
2. 表单使用
```php
$form->tencentlatlng('lat', 'lng', '选择位置');
$form->tencentlatlng('lat', 'lng', '选择位置')->height(600); //设置高度
$form->tencentlatlng('lat', 'lng', '选择位置')->zoom(19);//设置缩放
```



